//
//  ChooseBranchController.swift
//  College Communicator
//
//  Created by Yashaswi on 12/07/17.
//  Copyright © 2017 Yashaswi. All rights reserved.
//

import UIKit

class ChooseBranchController: UIViewController ,UIPickerViewDelegate,UIPickerViewDataSource{
    
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    var itemsBranches = ["CSE","ISE","MCE","ECE"]
    
    
    
    override
    func viewDidLoad() {
        super.viewDidLoad()
        print("Choose branch loaded")
        pickerView.delegate=self
        pickerView.dataSource=self
    }
    
    @IBAction func submitButtonClick(_ sender: UIButton) {
        let valueSelected=itemsBranches[pickerView.selectedRow(inComponent: 0)]
         print("Item selected : "+valueSelected)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1

    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return itemsBranches.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return itemsBranches[row]
    }
    
}
