//
//  ViewController.swift
//  College Communicator
//
//  Created by Yashaswi on 03/07/17.
//  Copyright © 2017 Yashaswi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var adminButton: UIButton!
    
    @IBOutlet weak var facultiesButton: UIButton!
    
    @IBOutlet weak var studentsButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
           print("view did load is called")
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("view will appear is called")
    }

    @IBAction func adminClick(_ sender: UIButton) {
        
        print("Admin button clicked")
    }
    
    @IBAction func studentClick(_ sender: UIButton) {
        
              print("Student button clicked")
        
        let alertController=UIAlertController(title: "iOS Alert", message: "This s an example of alert Popup", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(
            UIAlertAction(title: "OK",
                          style:UIAlertActionStyle.default,
                          handler: { action in self.alertOkClick()
                            
            }))
        alertController.addAction(
            UIAlertAction(title: "Dismiss",
                          style: UIAlertActionStyle.default,
                          handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func facultyClick(_ sender: UIButton) {
        
        print("Faculty button clicked")
    
        let actionSheet=UIAlertController(title: nil, message: "Choose options", preferredStyle: .actionSheet)
        actionSheet.addAction(
            UIAlertAction(
                title: "Delete",
                style: UIAlertActionStyle.default,
                handler: {

                    (alert: UIAlertAction!) -> Void in
            
                    print("Delete button is selected")
            }))
           
        actionSheet.addAction(
                UIAlertAction(
                    title: "Edit",
                    style: UIAlertActionStyle.default,
                    handler:
                    {
                        (alert:UIAlertAction!) -> Void in
                        print("Edit button is selected")
                }))
        
        actionSheet.addAction(
                    UIAlertAction(
                        title: "Cancel",
                        style: UIAlertActionStyle.default,
                        handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
   
    func alertOkClick()
    {
        print("Student alert OK  clicked")
    }
    

    
}

